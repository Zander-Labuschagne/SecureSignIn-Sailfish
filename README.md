# Secure Sign In Sailfish
This is a mobile application I have created in an attempt to improve my online account security, it is the Sailfish OS version of my Secure Sign In project. The user provides a password with an accompanying key that is unique to the website(or account) requiring the password, the application then provides a lengthy and complicated password to be used instead.

#### Benifits:
  - Remember one password for all sites, but all sites have different passwords.
  - Don't know the actual password which is entered in the password box on the website.
  - Provides a very strong, long and complex password.
  - No passwords are stored in file or database.
  - Easy to use.

This is my first application for Sailfish devices I have created. Feel free to criticize or comment on my code and practices.
There are desktop, Android and iOS applications available as well, however some are not always up to date and I have left some of them discontinued/incomplete. I work on these projects in my free time only so don't expect regular updates from me.

Desktop version: https://gitlab.com/Zander-Labuschagne/SecureSignIn-v4b

iOS version: *TBA*

Android version: https://gitlab.com/Zander-Labuschagne/SecureSignIn-Android-v3
  
The logo and name on the application comes from an iWorks template, it is not a registered company name or logo, I just added it to make it look impressive.

E-Mail: <zander.labuschagne@protonmail.ch>

Copyright (C) 2019 Zander Labuschagne. This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 2 as published by the Free Software Foundation.

#### Installation:
*TBC*

#### How to use application (also see screens below):
*TBC*

#### Screens (Still Android screens)
<img src="https://gitlab.com/Zander-Labuschagne/SecureSignIn-Mobile/raw/master/screens/front_end.png" alt="Main view. Enter password and key in text fields." width="300" align="middle"/>
<!--![alt text](https://gitlab.com/Zander-Labuschagne/SecureSignIn-Mobile/raw/master/screens/front_end.png =720x "Main view. Enter password and key in text fields.")-->
<img src="https://gitlab.com/Zander-Labuschagne/SecureSignIn-Mobile/raw/master/screens/front_end_compact.png" alt="Main view showing example. In this case the `compact password` option is enabled to shorten the password if necessary." width="300" align="middle"/>
<!--![alt text](https://gitlab.com/Zander-Labuschagne/SecureSignIn-Mobile/raw/master/screens/front_end_compact.png "Main view showing example. In this case the `compact password` option is enabled to shorten the password if necessary.")-->
<img src="https://gitlab.com/Zander-Labuschagne/SecureSignIn-Mobile/raw/master/screens/output_view.png" alt="Output view showing output of example. Copy the password to memory for pasting, or reveal the password for type over." width="300" align="middle"/>
<!--![alt text](https://gitlab.com/Zander-Labuschagne/SecureSignIn-Mobile/raw/master/screens/output_view.png "Output view showing output of example. Copy the password to memory for pasting, or reveal the password for type over.")-->

